﻿using System;
using System.Threading.Tasks;
using DataCore.Interfaces;
using Google.Cloud.Firestore;

namespace FirestoreDB
{
    public class FirestoreLoader : ILoadable
    {
        private static FirestoreDb _db;

        private static FirestoreDb Db => _db ?? (_db = FirestoreDb.Create("lexihedron"));//"cops-n-robbers-wt-staging"));

        public async Task<T> Interact<T>(Func<ITransaction, Task<T>> interaction)
        {
            return await Db.RunTransactionAsync(async transaction =>
            {
                var trans = new FirestoreTransaction(transaction);
                T result = await interaction(trans);
                await trans.BatchComplete();
                return result;
            });
        }

        public async Task Interact(Func<ITransaction, Task> interaction)
        {
            await Db.RunTransactionAsync(async transaction =>
            {
                var trans = new FirestoreTransaction(transaction);
                await interaction(trans);
                await trans.BatchComplete();
            });
        }

        public async Task<T> Gather<T>(Func<ITransaction, Task<T>> interaction)
        {
            return await interaction(new FirestoreTransaction(Db));
        }
    }
}