﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataCore.Classes;
using DataCore.DataSystem;
using Google.Cloud.Firestore;
using Timestamp = Google.Cloud.Firestore.Timestamp;
using Type = System.Type;

namespace FirestoreDB
{
    public static class FirestoreUtils
    {
        internal static Dictionary<Type, Func<FirestoreDb, Query>> Collections = new Dictionary<Type, Func<FirestoreDb, Query>>
        {
            { typeof(UserData), (db) => db.Collection("User") },
            { typeof(PuzzleMap), (db) => db.CollectionGroup("Credit") },
            { typeof(UserPuzzleData), (db) => db.CollectionGroup("Progression") },
            { typeof(ServerVersion), (db) => db.CollectionGroup("Version")},
            {typeof(VersionData), (db) => db.CollectionGroup("Association") }
        };

        internal static Dictionary<Type, Func<FirestoreDb, DataKey, DocumentReference>> Documents = new Dictionary<Type, Func<FirestoreDb, DataKey, DocumentReference>>
        {
            { typeof(UserData), (db, key) => db.Document($"UserData/{key.Keys[0]}") },
            { typeof(PuzzleMap), (db, key) => db.Document($"PuzzleMap/{key.Keys[0]}&{key.Keys[1]}") },
            { typeof(UserPuzzleData), (db, key) => db.Document($"UserData/{key.Keys[2]}/UserPuzzleMap/{key.Keys[0]}&{key.Keys[1]}") },
            { typeof(ServerVersion), (db, key) => db.Document($"Version/{key.Keys[0]}") },
            { typeof(VersionData), (db, key) => db.Document($"Association/{key.Keys[0]}") }
        };

        internal static DocumentReference GetDoc(DataKey key, FirestoreDb database)
        {
            return Documents[key.DataType](database, key);
        }

        internal static Dictionary<String, Object> GetDict(DocumentSnapshot doc)
        {
            var dict = doc.ToDictionary();
            StripDict(dict);
            dict.Add("ID", doc.Reference.Path.Split('/'));
            return dict;
        }

        internal static void StripDict(Dictionary<String, Object> dict)
        {
            var keys = dict.Keys.Where(property => dict[property] is Timestamp || dict[property] is Dictionary<String, Object>).ToArray();
            foreach (var property in keys)
            {
                if (dict[property] is Timestamp)
                {
                    dict[property] = ((Timestamp)dict[property]).ToDateTime();
                }
                else
                {
                    StripDict((Dictionary<String, Object>)dict[property]);
                }
            }
        }
    }
}
