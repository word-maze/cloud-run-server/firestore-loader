﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataCore.DataSystem;
using DataCore.Responses;
using DateTime = System.DateTime;

namespace FirestoreDB
{
    internal static class DataSystemTranslation
    {
        public static DataStorable Populate(Dictionary<String, Object> entity, Type dataType)
        {
            DataStorable result = (DataStorable)Activator.CreateInstance(dataType);
            switch (result)
            {
                case UserData data:
                    data.Populate(entity);
                    break;
                case PuzzleMap data:
                    data.Populate(entity);
                    break;
                case UserPuzzleData data:
                    data.Populate(entity);
                    break;
                case ServerVersion data:
                    data.Populate(entity);
                    break;
                case VersionData data:
                    data.Populate(entity);
                    break;
            }

            return result;
        }

        public static T Populate<T>(Dictionary<String, Object> entity) where T : DataStorable, new()
        {
            T result = new T();
            switch (result)
            {
                case UserData data:
                    data.Populate(entity);
                    break;
                case PuzzleMap data:
                    data.Populate(entity);
                    break;
                case UserPuzzleData data:
                    data.Populate(entity);
                    break;
                case ServerVersion data:
                    data.Populate(entity);
                    break;
                case VersionData data:
                    data.Populate(entity);
                    break;
            }

            return result;
        }

        public static Dictionary<String, Object> Destructure(DataStorable data)
        {
            switch (data)
            {
                case UserData specific:
                    return specific.Destructure();
                case PuzzleMap specific:
                    return specific.Destructure();
                case UserPuzzleData specific:
                    return specific.Destructure();
                case ServerVersion specific:
                    return specific.Destructure();
                case VersionData specific:
                    return specific.Destructure();
            }

            throw new ArgumentException();
        }
    }

    internal static class ServerVersionExtension
    {
        internal static void Populate(this ServerVersion data, Dictionary<String, Object> entity)
        {
            data.MajorVersion = (Int64)entity["MajorVersion"];
            data.MinorVersion = (Int64)entity["MinorVersion"];
            data.PatchVersion = (Int64)entity["PatchVersion"];
            data.BaseURL = (String)entity["BaseURL"];
        }
        internal static Dictionary<String, Object> Destructure(this ServerVersion data)
        {
            return new Dictionary<String, Object>
            {
                ["MajorVersion"] = (Int64) data.MajorVersion,
                ["MinorVersion"] = (Int64) data.MinorVersion,
                ["PatchVersion"] = (Int64) data.PatchVersion,
                ["BaseURL"] = data.BaseURL
            };
        }
    }

    internal static class UserDataExtension
    {
        internal static void Populate(this UserData data, Dictionary<String, Object> entity)
        {
            String[] path = (String[])entity["ID"];
            data.ID = path[0];
            data.Creation = (DateTime)entity["creation"];
            data.LastInteraction = (DateTime)entity["lastInteraction"];
            data.AvailableHints = (Int64)entity["availableHints"];
            data.Premium = (Boolean)entity["premium"];
        }
        internal static Dictionary<String, Object> Destructure(this UserData data)
        {
            return new Dictionary<String, Object>
            {
                ["premium"] = data.Premium,
                ["availableHints"] = data.AvailableHints,
                ["creation"] = data.Creation,
                ["lastInteraction"] = data.LastInteraction
            };
        }
    }

    internal static class PuzzleMapExtension
    {
        internal static void Populate(this PuzzleMap data, Dictionary<String, Object> entity)
        {
            data.Date = (String)entity["date"];
            data.Shape = (Int64)entity["shape"];
            data.Map = (String)entity["map"];
            data.CreatedWords = ((List<Object>)entity["createdWords"]).Select(x => new WordData((Dictionary<String, Object>)x)).ToArray();
            Dictionary<String, Object> foundWords = (Dictionary<String, Object>)entity["foundWords"];
            data.FoundWords = foundWords.ToDictionary(x => x.Key, x => new WordData((Dictionary<String, Object>)x.Value));
        }
        internal static Dictionary<String, Object> Destructure(this PuzzleMap data)
        {
            var createdWords = new Dictionary<String, Object>[data.CreatedWords.Length];
            for (var i = 0; i < data.CreatedWords.Length; i++)
            {
                createdWords[i] = data.CreatedWords[i].Destructure();
            }

            var foundWords = new Dictionary<String, Dictionary<String, Object>>();

            foreach ((String key, WordData value) in data.FoundWords)
            {
                foundWords.Add(key, value.Destructure());
            }

            return new Dictionary<String, Object>
            {
                ["date"] = data.Date,
                ["shape"] = data.Shape,
                ["map"] = data.Map,
                ["createdWords"] = createdWords,
                ["foundWords"] = foundWords
            };
        }
    }

    internal static class WordDataExtension
    {
        internal static void Populate(this WordData data, Dictionary<String, Object> entity)
        {
            data.Word = (String)entity["word"];
            data.Points = (Int64)entity["points"];
            data.Positions = (Int64[])entity["positions"];
        }
        internal static Dictionary<String, Object> Destructure(this WordData data)
        {
            return new Dictionary<String, Object>
            {
                ["word"] = data.Word,
                ["points"] = data.Points,
                ["positions"] = data.Positions
            };
        }
    }

    internal static class UserPuzzleDataExtension
    {
        internal static void Populate(this UserPuzzleData data, Dictionary<String, Object> entity)
        {
            data.ID = (String)entity["user"];
            data.Date = (String)entity["date"];
            data.Shape = (Int64)entity["shape"];
            data.FoundWords = ((List<Object>)entity["foundWords"]).Cast<WordData>().ToList();
        }
        internal static Dictionary<String, Object> Destructure(this UserPuzzleData data)
        {
            return new Dictionary<String, Object>
            {
                ["user"] = data.ID,
                ["date"] = data.Date,
                ["shape"] = data.Shape,
                ["foundWords"] = data.FoundWords
            };
        }
    }

    internal static class VersionDataExtension
    {
        internal static void Populate(this VersionData data, Dictionary<String, Object> entity)
        {
            data.MajorClientVersion = (Int32)(Int64)entity["MajorClientVersion"];
            data.MinorClientVersion = (Int32)(Int64)entity["MinorClientVersion"];
            data.PatchClientVersion = (Int32)(Int64)entity["PatchClientVersion"];
            data.ServerVersionDescription = (String)entity["ServerVersion"];
            data.ServerURL = (String)entity["ServerURL"];
        }
        internal static Dictionary<String, Object> Destructure(this VersionData data)
        {
            return new Dictionary<String, Object>
            {
                ["MajorClientVersion"] = (Int64)data.MajorClientVersion,
                ["MinorClientVersion"] = (Int64)data.MinorClientVersion,
                ["PatchClientVersion"] = (Int64)data.PatchClientVersion,
                ["ServerVersion"] = data.ServerVersionDescription,
                ["ServerURL"] = data.ServerURL
            };
        }
    }
}