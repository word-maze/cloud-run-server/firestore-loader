﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataCore.Classes;
using DataCore.DataSystem;
using DataCore.Interfaces;
using Google.Cloud.Firestore;
using Query = Google.Cloud.Firestore.Query;

namespace FirestoreDB
{
    public class FirestoreTransaction : ITransaction
    {
        public FirestoreDb Database;
        public Transaction Transaction;

        private readonly Dictionary<DataKey, DataStorable> _dataToStore;
        private readonly Dictionary<DataKey, DataStorable> _dataToDelete;



        public FirestoreTransaction(FirestoreDb database)
        {
            Database = database;
            Transaction = null;
            _dataToStore = new Dictionary<DataKey, DataStorable>(new DataKeyEqualityComparer());
            _dataToDelete = new Dictionary<DataKey, DataStorable>(new DataKeyEqualityComparer());
        }

        public FirestoreTransaction(Transaction transaction)
        {
            Transaction = transaction;
            Database = transaction.Database;
            _dataToStore = new Dictionary<DataKey, DataStorable>(new DataKeyEqualityComparer());
            _dataToDelete = new Dictionary<DataKey, DataStorable>(new DataKeyEqualityComparer());
        }

        public async Task<DataStorable> Get(DataKey key)
        {
            DocumentSnapshot doc;
            if (Transaction != null)
            {
                doc = await Transaction.GetSnapshotAsync(FirestoreUtils.GetDoc(key, Database));
            }
            else
            {
                doc = await FirestoreUtils.GetDoc(key, Database).GetSnapshotAsync();
            }

            if (!doc.Exists)
            {
                return null;
            }
            return DataSystemTranslation.Populate(FirestoreUtils.GetDict(doc), key.DataType);
        }

        public async Task<Dictionary<DataKey, DataStorable>> Get(params DataKey[] keys)
        {
            var docs = keys.Select(key => FirestoreUtils.GetDoc(key, Database));
            IList<DocumentSnapshot> snaps;
            if (Transaction != null)
            {
                snaps = await Transaction.GetAllSnapshotsAsync(docs);
            }
            else
            {
                snaps = await Database.GetAllSnapshotsAsync(docs);
            }
            var result = new Dictionary<DataKey, DataStorable>(new DataKeyEqualityComparer());

            for (Int32 i = 0; i < keys.Length; i++)
            {
                if (snaps[i].Exists)
                {
                    result.Add(keys[i], DataSystemTranslation.Populate(FirestoreUtils.GetDict(snaps[i]), keys[i].DataType));
                }
                else
                {
                    result.Add(keys[i], null);
                }
            }

            return result;
        }

        public async Task<List<T>> Query<T>(List<QueryFilter> parameters)
            where T : DataStorable, new()
        {
            Query query = FirestoreUtils.Collections[typeof(T)](Database);

            foreach ((String attribute, FilterType operation, Object value) in parameters)
            {
                switch (operation)
                {
                    case FilterType.EQUALS:
                        {
                            query = query.WhereEqualTo(attribute, value);
                            break;
                        }
                    case FilterType.LESS_THAN:
                        {
                            query = query.WhereLessThan(attribute, value);
                            break;
                        }
                    case FilterType.LESS_THAN_OR_EQUAL_TO:
                        {
                            query = query.WhereLessThanOrEqualTo(attribute, value);
                            break;
                        }
                    case FilterType.GREATER_THAN:
                        {
                            query = query.WhereGreaterThan(attribute, value);
                            break;
                        }
                    case FilterType.GREATER_THAN_OR_EQUALS_TO:
                        {
                            query = query.WhereGreaterThanOrEqualTo(attribute, value);
                            break;
                        }
                }
            }

            var list = (await query.GetSnapshotAsync()).Documents;

            return list.Select(doc => DataSystemTranslation.Populate<T>(FirestoreUtils.GetDict(doc))).ToList();
        }

        public async Task<T> Get<T>(DataKey<T> key) where T : DataStorable, new()
        {
            DocumentSnapshot doc;
            if (Transaction != null)
            {
                doc = await Transaction.GetSnapshotAsync(FirestoreUtils.GetDoc(key, Database));
            }
            else
            {
                doc = await FirestoreUtils.GetDoc(key, Database).GetSnapshotAsync();
            }

            if (!doc.Exists)
            {
                return null;
            }
            return DataSystemTranslation.Populate<T>(FirestoreUtils.GetDict(doc));
        }

        public void QueueSet(params DataStorable[] data)
        {
            QueueSet(data.AsEnumerable());
        }

        public void QueueSet(IEnumerable<DataStorable> data)
        {
            foreach (var entity in data)
            {
                if (!_dataToStore.ContainsKey(entity.Key))
                {
                    _dataToStore.Add(entity.Key, entity);
                }
                else
                {
                    _dataToStore[entity.Key] = entity;
                }
            }
        }

        public void QueueDelete(params DataStorable[] data)
        {
            QueueDelete(data.AsEnumerable());
        }

        public void QueueDelete(IEnumerable<DataStorable> data)
        {
            foreach (var entity in data)
            {
                if (!_dataToDelete.ContainsKey(entity.Key))
                {
                    _dataToDelete.Add(entity.Key, entity);
                }
                else
                {
                    _dataToDelete[entity.Key] = entity;
                }
            }
        }

        public async Task BatchComplete()
        {
            if (Transaction == null)
            {
                var batch = Database.StartBatch();

                foreach (var entity in _dataToDelete)
                {
                    batch.Delete(FirestoreUtils.GetDoc(entity.Key, Database));
                }

                foreach (var entity in _dataToStore)
                {
                    batch.Set(FirestoreUtils.GetDoc(entity.Key, Database),
                        DataSystemTranslation.Destructure(entity.Value));
                }

                await batch.CommitAsync();

                return;
            }

            foreach (var entity in _dataToDelete)
            {
                Transaction.Delete(FirestoreUtils.GetDoc(entity.Key, Database));
            }

            foreach (var entity in _dataToStore)
            {
                Transaction.Set(FirestoreUtils.GetDoc(entity.Key, Database), DataSystemTranslation.Destructure(entity.Value));
            }
        }
    }
}
